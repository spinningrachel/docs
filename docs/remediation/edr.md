Coro endpoint detection and response (EDR) enables the detection and analysis of suspicious activities on endpoints, enabling administrators to remediate issues whenever there appear to be problems with a specific endpoint. 

From the Coro console, administrators can remediate and mitigate by remotely: 

- blocking relevant suspicous processes 
- rebooting affected endpoints
- shutting infected endpoints off
- disconnecting relevant endpoints from the company's environments 

Find out more about EDR: 

- [Access EDR issues](#access-edr-issues) - navigate to and understand the EDR area
- [General elements](#general-eleements) - the overall layout of the area
- [Issues list](#issues-list) - finding issues in the list 
- [Issue details](#issue-details) - understanding all issue details 
- [Actions](#actions) - mitigating issues

## Access EDR issues

The **EDR** area in the Coro Console displays all potential Windows and MAC issues. 

**To access this area:**

1. Log in to the **Coro Console** and click ![EDR](../assets/screenshots/edr.png){ width=20px; display=inline } from the top right.

	The EDR area loads similar to the following: 

	![EDR](../assets/screenshots/edr-default.png)

	The default view displays all Windows endpoint issues. 

2. To view issues for endpoints with macOS, click the **macOS** tab: 

	![EDR](../assets/screenshots/edr-mac.png)

### General elements

The following table describes the general elements in this area:

| Element                | Description                                                                                                                                                                                   |
|------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Total number of issues | Appears next to the title of the area **Endpoint Detection & Response**                                                                                                                       |
| Endpoint type tabs     | <li> Windows - this is the default tab; lists all issues with Windows endpoints</li> <li>macOS - lists all issues with macOS endpoints </li>                                                                 |
| Filters                | Filter for specific issues as follows:  <li>Status - blocked or unblocked devices <li>Risk indicator - filter by the relevant risk factors <li>During - filter by dates that issues were identified |
| Search                 | Use free text to search for relevant issues                                                                                                                                                   |
| Issues                 | Lists all issues based on the relevant tab and filters; the description for the selected issue appears on the right side                                                                      |



### Issues list

The issues list appears as follows: 

![EDR](../assets/screenshots/edr-numbered.png)

The following table describes how to understand the list of issues.

| Number in image | Element             | Description                                                                                                                                                                                                            |
|-----------------|---------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1               | Number  | The left side of the issue list displays the total number of suspicious processes identified.                                                                                                                          |
| 2               | Sort  | The default sort is by the number of devices affected.   From the dropdown, change the sort according to:   <li>last seen <li> blocked on - the date on which you blocked the device from the network  <li> risk index |
| 3               | Sort direction      | Click to change sort between ascending and descending.                                                                                                                                                                 |
| 4               | Issue details       | The right side of the list displays the issue details for the currently selected issue, as described in [Issue details](#issue-details).                                                                               |


### Issue details

The following table describes the details available in each issue: 

| Element         |            | Description                                                                                                                                                                                            |
|-----------------|------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Actions         |            | Remediate issues as follows:  <li>Reboot infected devices <li>Shut down infected devices <li>Block the suspicious processes <li>Block the relevant endpoints from access to the company's environments |
| Title           |            | The name of the process                                                                                                                                                                                |
| Process details |            | Additional process details                                                                                                                                                                             |
| Risk index      |            | A list of related risk factors                                                                                                                                                                         |
| Devices         |            | Number of affected devices                                                                                                                                                                             |
| Device details  | Search     | Free text to search for properties and instances                                                                                                                                                       |
|                 | Properties | Lists of related properties and property details                                                                                                                                                       |
|                 | Instances  | Details for each time the suspicious process was identified                                                                                                                                            |

## Actions

From the **EDR** area, you can: 

- block relevant suspicous processes 
- reboot affected endpoints
- shut infected endpoints off
- disconnect relevant endpoints from the company's environments 

**To remediate any issue:**

1. Navigate to the **EDR** area.

2. Click the tab for the relevant operating system and then find and click the issue that you would like to handle. 

3. From the **Actions** menu, select the relevant action that you'd like to perform. This action is performed on all related devices. 

	A confirmation message pops up notifiying of the total number of devices that will be affected. 

4. Click **Confirm** to execute the operation.