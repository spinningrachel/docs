
An Activity Log is available for every workspace and displays all actions that the administrator of this workspace performed in it.

View the Activity Log by clicking **Activity Logs** in the toolbar.

![Dashboard](../assets/screenshots/activity-log.png)

- Where a link is provided in an activity, you can click it to see more details about it.
- Where an Undo button is provided for an activity, you can click it to undo the action that was performed.