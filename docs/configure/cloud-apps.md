You can set access permissions for drives associated with Microsoft 365, Google Workspace, Salesforce, Dropbox, and Box. 

Access the relevant screen by clicking **Control Panel** > **Access Permissions Violations**.
 
For Coro to monitor and report security issues, at least one Cloud application must be connected.

These Cloud applications are currently supported:

- Microsoft 365
- Google Workspace
- Slack
- Dropbox
- Box
- Salesforce