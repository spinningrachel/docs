Coro Agent provides protection for the devices in your organization. 

- Coro Agent works like any other standard virus application: it checks the hashes of files that are downloaded against public databases of already-checked viruses; if they match, Coro puts the file in quarantine. 

- In addition, Coro Agent detects additional vulnerabilities on PCs. For example, it checks that the computer has a password and that Developer Mode remains turned off.

   <!-- 4 FUTURE - "NextGen AV" 
   It has an additional behavior: EDR 
   It performs additional levels of protection, such as: analyzes every process activities that take place in the PC. If it detects suspicious behavior, Coro alerts the admin about it. __This feature is in development still.__ -->

Coro Agent also works on devices that are not within your company domain and do not consume Cloud data {TBD: According to a talk I had with Carmel, a contractor who does not have a company email does not need endpoint protection}. All that is required is that the device belong to a protected user and that the device itself be protected, meaning that it run an endpoint agent that has been [installed using a link that is specific to the workspace](../installation/coro-agent.md#deploy-coro-agent).

The value of device protection is quite obvious. Here are several typical scenarios that demonstrate how your organization cannot do without it:

- Compliance requirements often call for computer drives that the organization is working with, whether internally or externally to be encrypted. If the contractor's device does not have Next-Generation Antivirus software installed, Coro is an ideal solution.
- An admin who knows that an endpoint agent is installed on a device is more at-ease about the user keeping sensitive information on that device.

## Turn Coro Agent on and off

Coro Agent is on, by default, immediately after installation on the device. To turn it off:

1. Display the actionboard.

2. Click **Devices**.

3. In the left panel of the Devices area, click **View**.

4. Click the specific device. Then select **Actions** > **Remove from protection**.

To return protection to the device, see [Add users to protection](../get-started/users-roles.md#add-users-to-protection).

## Make Coro Agent settings

In addition to it basic functions, Coro provides specific endpoint monitoring capabilities that the admin can turn on and off. To control the settings, click **Control Panel** >  **Endpoint Devices** > **Settings**. 

The settings apply to all devices in the workspace.

For more information on the settings, see [Endpoint device settings](../configure/endpoints.md#settings).

## Requirements for Coro Agent deployment

Desktop PCs must meet the following requirements:

### Operating system

**Windows OS**

- Windows server 2012 R2, Windows server 2016, Windows server 2019
- Windows 7 , 10 , 11 64 Bit

**macOS**

macOS 11, at least Big Sur


### Other system requirements

Memory - 8GB 

Size - 126MB

## Deploy Coro Agent

There are two methods for installing Coro Agent.

- **For single users**: Coro provides a link to the installer. Admin personnel email the link to specific users on whose device it needs to be installed. The user clicks the link and follows the instructions to install.
- **For use with mass deployment tools, such as GPO**: Admin staff use the same link to download the Agent, then deploy it en masse using the deployment tool. 

Either method can be used for devices running Windows and macOS.

See also: [Agent deployment](../configure/endpoints.md#agent-deployment)

## Upgrade Coro Agent

The following are instructions for upgrading Coro Agent on computers running Windows.

| Version currently installed | Action |
| --- | --- |
| 2.0.27.0 and later | Users can upgrade Coro Agent to the newer version by downloading the new file and installing it. First, they must disable tamper protection in the workspace. |
| Earlier than 2.0.27.0  | Users must uninstall the old agent, download the newest version and install it. Before uninstalling, they must first disable tamper protection in the workspace. |
| 2.0.30.0 and later | End-users receive auto-update notifications. If administrators need to download the latest version and upgrade, or perform mass deployment using the file), they must first disable tamper protection in the workspace. |