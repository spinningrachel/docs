# Install Coro add-in for Outlook

The purpose of the Coro add-in for Microsoft 365 is to allow end users to report to their security personnel on suspicious emails in their inboxes,
as well as to report on emails that have been flagged by the Coro protection and moved to the Suspected folder when the user believes that the
flagging was unjustified.

The feedback provided by the end users via the add-in is communicated to the
Coro administrators via the Coro **Actionboard** as part of the Email security tickets that
require review. 

After examining the ticket, the administrator may decide to adopt user feedback by approving or discarding the
respective email, or allowlisting/blocklisting the sender of the email in question, or even allowlisting/blocklisting the entire email *domain* of
the sender.

## To install the add-in for all users in the workspace

As a Coro administrator, you can install the add-in for all your Microsoft 365 users at once, transparently to them. 

1. Make sure you are logged into Microsoft 365 with your global admin credentials.

2. In your browser, go to the Coro page in the Microsoft [AppSource](https://appsource.microsoft.com/en-US/) site.

    ![MSAppStore](../assets/screenshots/msappstore.png)

3. Click **Get it now**.

4. Accept the requested permissions.

    ![Accept-permission](../assets/screenshots/outlook-add-in-permissions.png)

    End users now see the Coro icon within their Outlook app for the web and desktops.

    ![Add-in-user](../assets/screenshots/outlook-addin-3.png) 

5. An instructions panel is displayed when the user clicks the Coro icon, letting users report phishing or mark emails as safe.

    ![Add-in-user](../assets/screenshots/add-in-user-1.png)

When the user submits a report, the **Coro Console** updates the **Ticket Log** with the request. 

See more details about using the Coro add-in [here](../installation/outlook-addin.md).