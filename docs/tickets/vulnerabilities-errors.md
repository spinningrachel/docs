---
template: overrides/main.html
---
This page lists the violations and errors for which Coro triggers tickets for different areas in your organization.

## Cloud apps

- Abnormal admin activity
- Malware in cloud drive
- Suspected identity compromise


## Users

- Access permission violation
- Mass data deletion
- Suspicious exposure of certificate
- Mass data download


## Emails

- Malware in email attachment
- Email phishing

## Devices

- Firewall disabled
- UAC notification missing
- Device password missing
- Malware on endpoint
- Coro protection updates missing
- Encryption disabled
- Anti-virus disabled
- Development mode enabled
- Non-genuine Windows copy
- Tamper resistance disabled
- Infected process
- Unencrypted endpoint drive
- Coro protection updates missing
- Windows updates disabled


## Data

- PII (personally identifiable information)
- PHI (protected health information)
- PCI (payment card information)

