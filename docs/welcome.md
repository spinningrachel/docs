<base target="_blank">


# Welcome to Coro

Coro offers enterprise-grade security to mid-market businesses, at affordable costs. Our goal is to bring cybersecurity to your company through a simple, minimalistic unified action board, so that you can analyze your security posture at a glance and drill down to details quickly and easily.

With open APIs, extensive third-party integrations, and consolidated dashboards and alerts, Coro makes cybersecurity easier and more effective.

<!-- ## How it works -->

<!-- Add here what Evgeny explained about Coro being an aggregator of data coming from Cloud apps. then being shown and handled by Coro. -->
<!-- TBD - complete Ask Carmel and Rachel -->

<!-- ## In this documentation set -->

<!-- You'll find a collection of articles on the Coro user interface, an API reference etc  TBD -->

We're on GitLab! Go to [develop\pmm-nd-docs](https://gitlab.com/coronet/bugatti/product/pmm-nd-docs).

If there's something you'd like to contribute, fork the project and submit your suggestions. We're eager for your feedback. 

## More learning resources

Coro has a blog! Check out our articles [here](https://www.coro.net/blog/).

<!-- Cyber topics TBD

View tutorials on YouTube TBD -->

<!-- Put here all the resources the user has: docs, blogs, tech support -->


## Technical support

To contact our support desk, shoot us an email [here](https://support.coro.net/hc/en-us/requests/new or email support@coro.net).