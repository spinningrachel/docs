Users can view details of their accounts by clicking their avatar at the top right area of the screen, and selecting **My Account**.


## Profile

This tab shows basic profile information.

Click **Update Profile** to edit profile details 

## Password

This tab shows the current password and lets users change their password.

## Two factor authentication

This tab display the 2FA status and the recovery mobile number. Users can delete 2FA data here.

## Notifications

Users can select the types of notifications they want to receive my email.
