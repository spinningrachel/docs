This page describes general settings for the workspace. Click the gear icon in the upper right corner of your screen.

TBD: image? 

To view and edit [settings for your personal account](../manage/my-account.md), click the avatar in the top right corner.

TBD: image? 
TBD: What level of account settings are these? On the workspace level? How is this different from My Account?


## Admin users

This screen displays the admin users of the workspace. The following details are provided:

- Name
- Email
- Status
- Status of two-factor authentication

In this screen, you can also:

- Activate two-factor authentication for admin users
- Add admin users
- Remove admin users
- Inspect the content inspection permissions of the admin users (TBD: is this used? Ask: Yoav what this is.)

## Active sessions

This screen displays the devices from which admin users have logged into the system. The following details are provided:

- IP address, with application and operating system
- User
- Country
- Date of sign-on
- Data when last accessed

Click the **Revoke** button to revoke the current session for a user.

## Billing

This screen displays the current billing plan for the workspace. There are buttons to update the plan and to add payment methods.