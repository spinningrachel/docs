
*   All files detected as malicious are automatically moved to a dedicated quarantined folder; no further remediation action on the part of the operator is required. At the same time, after examining the ticket, the operator may decide to approve the respective file, or to permanently delete it. Therefore, all cloud app malware tickets are classified as suggested for review (G2), with a review time window of two weeks.
*   Suspected Identity Compromise and Abnormal Admin Activity tickets are classified as suggested for review (G2), and are automatically closed after the review period of two and four weeks, respectively.
*   Suspected Bot Attack tickets are classified as G3, being automatically closed and logged immediately. 

See also: [Ticket classification](../remediation/ticket-classification.md)