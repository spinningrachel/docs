---
template: overrides/main.html
---


Privacy compliance laws prevent Coro technical support agents from accessing your workspace.

Should you require technical support, you can temporarily enable Coro staff to inspect your workspace by turning on **Grant access to Coro technical support agents**.

![Grant login](../assets/screenshots/grant-login-access-button.png){ align=left }