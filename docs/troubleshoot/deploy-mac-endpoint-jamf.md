---
template: overrides/main.html
---

This guide walks you through the process for deploying Coro to macOS endpoints using Jamf Pro. 

!!! note
    To successfully deploy Coro to Mac endpoints via Jamf, you must create and deploy a configuration profile, a package, and a policy in Jamf Pro. 

## Prerequisites

You must download Coro .pkg file from the Coro admin console.

1. Click the gear icon in the top-right corner.
2. Click **Endpoint Devices**.
3. Copy Download Link and download locally on macOS machine to upload to Jamf Pro**

Ensure you have a working Jamf environment configured for their endpoints that allows deployment of packages as well as configuration profiles. 

## Step 1 - Configure Coro System Extension Profile in Jamf Pro

1.  In Jamf Pro, go to **Computers** > **Configuration Profiles** > **New**.
2.  Create a new Configuration Profile and give it a name such as "*Coro System Extensions*".
3.  Scroll down and select **System Extension** in your new configuration profile.
4.  In the dialog that opens, configure the profile with the following settings:
    1.  Display Name: Coro App Bundle
    2.  System Extension Type: Allowed System Extensions
    3.  Team Identifier: E3P52EVK39
    4.  Allowed System Extensions:
        1.  coro.endsec.CoroService
        2.  coro.endsec.Coro
    5.  Example ***System Extensions Profile* ***-* 
1.  Save the configuration profile and scope the configuration profile to the macOS endpoints that need Coro. 

## Step 2 - Configure Coro Full Disk Access Configuration Profile in Jamf Pro

1.  In Jamf Pro, go to **Computers** > **Configuration Profiles** > **New**.
2.  Create a new Configuration Profile and give it a name such as "*Coro System Extensions*".
3.  Scroll down and select **Privacy Preferences Policy Control** in your new configuration profile.
4.  In the dialog that opens, configure the profile with the following settings:
    1.  Identifier: net.coro.endsec.Coro
        
        ***Note, you'll create a duplicate entry when the identifier is coro.endsec.CoroService as well!***

    2.  Identifier Type: Bundle ID
    3.  Code Requirements:
        1.  anchor apple generic and identifier "net.coro.endsec.Coro" and (certificate leaf[field.1.2.840.113635.100.6.1.9] /* exists */ or certificate 1[field.1.2.840.113635.100.6.2.6] /* exists */ and certificate leaf[field.1.2.840.113635.100.6.1.13] /* exists */ and certificate leaf[subject.OU] = E3P52EVK39).
    4.  Click **Add** next to **APP OR SERVICE** and select **System Policy Files**. Set Access to **ALLOWED**.
    5.  Example ***Coro ALLOW Full Disk Access PPPC Profile***

1.  Save the configuration profile and scope the configuration profile to the macOS endpoints that need Coro.

## Step 3 - Configure Coro Notifications ENABLED Configuration Profile in Jamf Pro

1.  In Jamf Pro, go to **Computers** > **Configuration Profiles** > **New**.
2.  Create a new Configuration Profile and give it a name such as "*Coro System Extensions*".
3.  Scroll down and select **Notifications** in your new configuration profile.
4.  In the dialog that opens, configure the profile with the following settings:
    1.  App: Coro\ Endpoint\ Protection.app
    2.  Bundle ID:coro.endsec.Coro
    3.  *Settings*
        1.  Critical Alerts - **ENABLED**
        2.  Notifications - **ENABLED**
            1.  Banner Alert Type - **Temporary**
            2.  Notifications on Lock Screen - **Display**
            3.  Badge App Icon - **Display**
            4.  Play Sound for notifications - **Display**
        3.  Example ***Coro Enforce Notifications Configuration Profile***

1.  Save the configuration profile and scope the configuration profile to the macOS endpoints that need Coro. 

## Step 4 - Upload Coro Package in Jamf Pro

1.  In Jamf Pro, go to **All Settings** > **Computer Management** > **Packages** > **New**.
2.  Create a new Package and under **File Name**, upload the .PKG file you downloaded from your Coro admin console. That action populates all necessary information for your new package automatically.
3.  Example **Package** for Coro --  

Next, you'll need to create a policy to deploy your .PKG file

## Step 5 - Create a Policy to Deploy Coro in Jamf Pro

1.  In Jamf Pro, go to **Computers** > **Policies** > **New**.
2.  Create a new Policy and give it a name such as "*InstallCoro*".
    1.  Configure Policy Trigger so:
        1.  ***Login***
        2.  ***Enrollment Complete***
        3.  ***Reoccurring Check-in***
    2.  Configure Execution Frequency. Make the configurations according to what is best for your needs.
        1.  ***Once per Computer***
            1.  Automatically re-run upon failed attempt (3 retry attempts).
        2.  User Interaction:
            1.  Deferral
                1.  ***No Deferral Type***
            2.  Example Policy to **deploy Coro .PKG and force install**



## Step 6 - Verify Successful Installation of Coro on macOS

-  On the macOS endpoint, you should see Coro as an application within the
Launchpad, as well as within the system tray.

- On the Coro Admin Console, you should see your machine under **Devices**.

1.  1.  Example endpoint in Coro Admin Console -