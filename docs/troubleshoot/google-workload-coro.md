If you experience difficulties when trying to connect Google Workspace (previously: G-Suite) to Coro, troubleshoot by taking the following steps:

## Make sure you're a super admin

Only a super admin in Google Workspace accounts can enable access to third parties.

1.  Log in to your [Google Admin console](https://admin.google.com) using a super admin user account.
2.  Navigate to **Directory/Users**, filter the user list by "Admin role: Super admin" and make sure you are in the list:

    ![](../assets//screenshots/google-coro-1.png)

## Review access control settings

Ensure that Coro has been allowed the correct access.

1.  In the Google Admin console, navigate to [**Security** \> **Access and data control** \> **API Controls**](https://admin.google.com/ac/owl)
2.  Make sure that:
    * The **Block all third-party API access** option is unchecked.
    * The **Trust internal, domain-owned apps** option is checked.
    
    ![](../assets//screenshots/google-coro-2.png)
    

## Add Coro as a trusted app

Ensure Coro is registered as an OAuth trusted app.

1. From **Security** > **Access and data control** > **API Controls**, click **MANAGE THIRD-PARTY APP ACCESS**.

    ![](../assets//screenshots/google-coro-3.png)

2. Click **Add App** and select the **OAuth App Name or Client ID** option:

    ![](../assets//screenshots/google-coro-4.png)

3. Search for “Coro” and select the Coro app:

    ![](../assets//screenshots/google-coro-5.png)

4. In the next Client ID screen, check Coro client ID and click **SELECT**:

    ![](../assets//screenshots/google-coro-6.png)

5. In the next App Access screen, select **Trusted: Can access all Google services** and click **CONFIGURE**:

    ![](../assets//screenshots/google-coro-7.png)

!!! note
    If the app is already configured, you might get an error message at this stage.

## Reconnect to your Google Workspace account

Go back to the Coro login page and try to reconnect to your Google Workspace service.

!!! note
    Google updates their UI/UX from time to time, without alerting developers or users. Contact [support@coro.net](mailto:support@coro.net) if the above screenshots or directions look different from what you're seeing in your Google Admin portal.