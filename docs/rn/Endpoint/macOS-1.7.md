Coro Agent for macOS, version 1.7 provides new features, enhancements, and fixes:

## New features

- EDR
- Shutdown and reboot device actions
- Drive encryption
- Firewall remediation support both manual and automatic
- Bumped AV library to latest version

## Enhancements

Internal bug fixes