
Email security issues are handled as follows:

Tickets associated with user phishing/safe feedback provided via Coro add-ins, spoofing of important domains (including customer’s domains), and impersonation of specific users of the customer - these tickets are classified as G2, suggested for review, and are automatically closed after the review period of two weeks.

All other flagged incoming emails are deleted or removed from the Inbox and moved to the Suspected folder, are classified as G3, and are immediately closed by the system. 

----------

[TBD- Carmel: This was discussed and flagged recently, but I am not sure if it is the only thing to say about phishing, or it's in addition to what I have above]

- If domain is block listed -> block
- If the domain is allow listed:
    - If the sender is block listed -> block; otherwise -> pass without inspection.
- If the domain is neutral:
    - If the sender is block listed -> block;
    - If the sender is allow listed -> pass without inspection;
- If the domain is neutral -> inspect.

--------------
Alternative:

- If you have added the domain abc.net (an example) in the block list, all phishing emails that come from anc.net will go to the
suspected folder.
- If you set the domain abc.net in the block list, all phishing emails from abc.net will be deleted.
- If you have not set abc.net in the block list, but have added joe@abc.net to the phishing allow list, all phishing emails coming from
sendabc.net, except those from joe@abc.net will be sent to the suspected folder and joe@abc.net emails will be
delivered.
- If abc.net is in the block list and joe@abc.net is in the allow list, all emails will be deleted.

See also: [Ticket classification](../remediation/ticket-classification.md)