!!! note
    This feature is supported on installed drives only. It is not yet available for removable drives.

Coro lets you remote encrypt disks on endpoint devices.

The device does not have to be online at the time you apply encryption. Once you set encryption in motion, the next time the device comes back online, it will be completed.

## Encrypt a drive - MS Windows

Coro applies disk encryption in computers that are running on MS Windows using BitLocker. You do not have to interact with BitLocker during the process.

1. For a device of interest, under **Drives**, click the **Unencrypted Endpoint Drive** link.

    The Ticket Log is displayed.

2. Select **Actions** >  **Encrypt drive**.

    !!! note
        If multiple drives must be encrypted, the order of encryption is: first the drive where the operating system is installed and then other drives.


3. In the confirmation that is displayed, click **Yes**.

    !!! note
        Select the **Close ticket upon confirmation** checkbox to instruct Coro to automatically close the ticket once the device is encrypted. 

- The Activity Log updates with a notification that drive encryption has been requested for the device. Following verification that the drive can be encrypted, Coro performs or aborts the encryption and updates the notification either to "Encryption succeeded" or "Encryption failed".
- For your convenience, the recovery key for accessing the encrypted data appears in the Activity Log.

    ![Encrypted drive notification](../assets/screenshots/encrypted-drive-notification.png)
<!--
## Encrypt a drive - macOS

Coro applies disk encryption in computers that are running on macOS using FileVault.

Follow the procedures that are described in [Encrypt a drive - MS Windows](#encrypt-a-drive---ms-windows).

When disk encryption has been requested in Coro for a Mac computer, encryption will be applied once the Mac user performs the following procedures:

1. Restarts the Mac.
2. Provides a password to enable FileVault on the computer.

    ![Encrypted drive notification](../assets/screenshots/mac_filevault_authentication.png)

!!! note
    In the event that the Mac user dismisses the dialog without providing a password, the Mac restarts, then prompts the user to enable FileVault.

    ![Encrypted drive notification](../assets/screenshots/mac_filevault_enable.png)
-->

## Check summary status of all the devices in a workspace

You can quickly find the devices that have unencrypted drives status in several ways. Here is one:

1. In the actionboard, click **Devices**.
   ![Device status](../assets/screenshots/device-status.png)
2. Click the **View** link.

3. Click through individual devices to find **Unencrypted Endpoint Drive** detection. For example:
   ![Unencrypted drive](../assets/screenshots/unencrypted-drive.png)

## Filter on unencrypted endpoint drives

You can filter the devices in a workspace that are unencrypted.

Use this method to show all the devices in a workspace that have unencrypted drives.

1. In the actionboard, click **Devices**.

2. From the filters along the top of the screen, select **Vulnerability** > **Unencrypted Endpoint Drive**.

    Only those devices with unencrypted drives are displayed. 
   ![Filter unencrypted drive](../assets/screenshots/filter-unencrypted-drive.png)