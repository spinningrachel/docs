# Use the Coro Outlook add-in

The Coro add-in for Outlook lets users report:

- Purportedly malicious emails that have been missed by Coro's protection (see [Report emails as phishing](#report-emails-as-phishing))
- Emails that have been flagged by Coro but which the user believes in error (see [Report emails as safe](#report-emails-as-safe))

!!! tip
    The rest of this page describes how the user can report these emails to you through the **Coro Console**. Share this information with them if they have any questions! 

## Report emails as phishing

1. Click the **Coro** button from the Outlook toolbar.

    ![Add-in-user](../assets/screenshots/coro-button.png)

    The side panel opens, similar to the following:
    
    ![Add-in-user](../assets/screenshots/add-in-user-1.png) 

    Now, you can [report phishing or mark emails as safe](../installation/outlook-addin.md).  

4. Click **Report Phishing**.

    ![Coro add-in icon](../assets/screenshots/coro-addin-dialog.png)

    A notification **Marked as phishing** appears in the email in Outlook and a ticket is generated for the administrator in their **Ticket Log**:

    ![Safe email](../assets/screenshots/email-phishing.png)

    The feedback provided by the users through the add-in is reflected in the [Ticket Log](../tickets/ticket-log.md).

## Report emails as safe

1. Click the **Coro** button from the Outlook toolbar.

    ![Add-in-user](../assets/screenshots/coro-button.png)

    The side panel opens, similar to the following:
    
    ![Add-in-user](../assets/screenshots/add-in-user-1.png) 

    Now, you can [report phishing or mark emails as safe](../installation/outlook-addin.md).  

4. Click **Mark As Safe**.

    ![Coro add-in icon](../assets/screenshots/coro-addin-dialog.png)

    A notification **Mark As Safe** appears in the email in Outlook and a ticket is generated for the administrator in their **Ticket Log**:

    ![Safe email](../assets/screenshots/email-phishing.png)
    
    The feedback provided by the users through the add-in is reflected in the [Ticket Log](../tickets/ticket-log.md).