[TBD: This content should be moved into a larger topic]

Coro generates email tickets, even for unprotected users. These tickets are marked with the symbol as shown here:

![Unprotected user](../assets/screenshots/user-not-protected.png){ align=left }

In the event that such a ticket is reported, the administrator can add protection for the user by clicking **Add Protection** in the detail pane.

See also: [Protected users](../configure/protected-users.md).