This page describes protected and unprotected users as well as administrator types. 

Coro defines the following user types: 

- [Protected and unprotected users](#protected-and-unprotected-users) together comprise all of the users that you manage in your company 
- [Administrators of Coro](#administrators) are the admins that work together with you from the Coro platform

## Protected and unprotected users

Coro defines protected and unprotected users as follows:

| User type        | Description                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ---------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Protectable user | A user that is listed in your company's directory (in Microsoft 365 or Google Workspace), has an email box and {==is properly licensed in the respective app==}{>>if a user who is in the directory has a pirated license, this means they can't be protected; but that's not true b/c Coro also alerts on pirating since this leaves the OS exposed to security breaches<<} (for example, has a 365 license)                                                                                                                                                                                                                                                            |
| Protected user   | A user that is added to Coro for monitoring and remediation actions, either individually or via a group. Note that a protected user does not have to be a protectable user {==(for example, the customer may want to extend the protection to the endpoint of a sub-contractor that is not listed in the directory or to an employee that has no 365 license but, for instance, has an endpoint, or to an general purpose endpoint such as we have at some customers).==}{>>Moshe since contractors can be protected, why are we defining protectable as only including people in the company's directory? << Carmel answered this question: if a contractor does not have a company email, there is no rationale for adding him to protection (n contrast to what was correct about a year ago)} 
| Unprotected user | An unlicensed user; thus one that was not added to protection. ### Protected and unprotected users

Let's say a paying customer has a workspace with one thousand users, wherein only 100 are protected.

- Coro generates tickets for all one thousand users - protected and unprotected. If the admin inspects the Ticket Log, you'll see tickets for all users.
- However, the actionboard singles out only protected users and a ticket is remediated by the system or can be acted on by the admin only if it involves protected users.
- If a ticket involves both protected and unprotected users, the action will work only for the protected user. For example, assume that a phishing email is sent to Bob (protected) and Alice (unprotected); for Bob, the system will move the email into the Suspected folder but for Alice not. 



### Add users to protection
Users can be added to protection in one of the following ways:

- Individually, by clicking the **Add to Protection** action inside a ticket. See [Ticket log](../tickets/ticket-log.md).
- Individually or in groups, under **Control Panel** > **Protected Users**. See [Protected users](../configure/protected-users.md).

### Remove users from protection
Users can be removed from protection in one of the following ways:

- Individually or in groups, under **Control Panel** > **Protected Users**. See See [Protected users](../configure/protected-users.md).
- Automatically, when a user who is removed from a protected group isn't a member of any other protected group.

	Note the following principles with regard to groups and individual user protection:

	- When a group X is removed from protection, a user that belongs to the group is removed from protection only if there is no other protected group Y that the user belongs to AND if the user has not been added to protection individually.
	- When a user is removed from a protected group X, then the user becomes unprotected if there is no other protected group Y that the user belongs to AND the user has not been added to protection individually.
	- A user X can be removed from protection individually ONLY if there is no protected group that the user belongs to. 


## Administrators

There are two levels of administrator permissions possible in Coro.

| Admin          | Permissions                                                                                                                                                                       |
|----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Channel admin  | Created by invitation admin in channel workspace. Can also be admin in a related child workspace. Can create child workspaces and edit them.                              |
| Regular admin | User in a regular workspace or child workspace. Can see only the workspace where this user is an admin. Cannot create any other workspace and cannot edit this workspace. |
