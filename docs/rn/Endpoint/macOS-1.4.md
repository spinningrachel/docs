Coro Agent for macOS, version 1.4 provides new features, enhancements, and fixes:

## New features

- There is a brand new UI.
- Coro Agent can now run on both Intel and Apple silicon CPUs as a native app (instead of adaptive to new OS).
TBD: - Realtime, on access to file, malware detection
- This version provides support for Apple silicon CPU native malware detection.
- In this version, there is support for Intel CPU native malware detection.
- Now, malware detection is done by system extension.
- Malware analysis and remediation is done on a system level.
- The app name has changed to "Coro Endpoint Protection".
- The new nextgen Agent can be installed on the same device as the legacy Coro Agent and the SecureCloud Agent - the nextgen Agent will remove the others when installed.

## Enhancements

Protection starts from the moment of bootup.

## Known issues

- Some vulnerabilities are not detected:
    - FileVault
    - Password missing
    - Auto-update is not available in this version. Due to the new architecture, manual download and install are required.
    - Initial and remote scans do not scan for files on drive.
    - Due to native browser behavior, the "Approve file as not malicious" mechanism does not work for files that were downloaded from a that browser. 