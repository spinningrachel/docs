---
template: overrides/main.html
---
[TBD: This is incomplete
Ira: doens't think this is good. It can change and also incomplete. Where are "white" and how do they fit in with G1 G2 G3?]

This page documents the kinds of tickets created by Coro for different violations, suspicious activities and more that the system detects.


**Grey tickets**

-   AR with sign out
-   AR with suspend
-   PII non-critical
-   PHI non-critical
-   PCI non-critical
-   All devices vulnerabilities except Malware on Endpoint with setting Always remediate
-   All devices vulnerabilities except Malware on Endpoint with setting Ignore (without remediation)
-   Massive download
-   Suspected exposure of source code
-   Suspected Bot attacks

**Red tickets**

-   Malware on cloud drive
-   All other device vulnerabilities with seeing Review
-   Abnormal admin activity
-   Suspected identity compromise
-   Email phishing
-   Malware in Email
-   Email marked by add-in as phishing
-   Email marked by add-in as safe
-   Massive delete
-   AR without remediation
-   PCI critical
-   PII critical
-   PHI critical
-   Suspected exposure of critical data
-   Suspected exposure of certificate
-   Suspected exposure of password