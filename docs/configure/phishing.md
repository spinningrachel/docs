
In this screen, configure email phishing settings. Access the screen by clicking **Control Panel** > **Phishing**.

## Settings tab

You can turn on or off protection against any of the following email phishing types:

- Suspicious impersonation
- Suspicious links and attachments
- Phishing-type content


### User Feedback

The Coro add-in for Outlook and Google email applications lets users provide feedback on inaccurate email classification. See [Mark emails as safe](../installation/outlook-addin.md#mark-emails-as-safe).


The links in this area of the control panel let admins quickly deploy the Coro agent on devices in their workspace.

### WiFi Phishing**

<!-- This is slated to be removed soon-->

Detect connections to suspicious WiFi access points that can be used to steal user data, including login credentials and credit card numbers.

## Allow/Block tab

This area shows the lists of domains that you trust  (allow) and do not trust (block).

- Click **Remove** to remove the selected domain from the allow list.
- Click **Add** to add a domain and designate it as allowed or blocked. 