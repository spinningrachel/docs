Coro Agent for Windows, version 1.7 provides new features, enhancements, and fixes:

## New features

- Redesigned UI to allow the user better visibility
- Drive encryption

## Enhancements

- In previous versions, the ATC driver was not removed on uninstall. This has been fixed.
- Beginning with this release, uninstall does not require reboot.