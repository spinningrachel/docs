
Some basic Coro concepts will help you find yourself around the product:

### Cloud apps

Cloud apps are applications hosted in the cloud by a third-party; for example Google apps, Microsoft 365 apps, Dropbox, or Box. 

### DLP

DLP is data loss protection.

### Endpoint

Endpoints include any device that has access to the company's network.

### EDR

Endpoint detection and response (EDR) is a mechanism by which Coro monitors your endpoints, mitigates potential threats, and remediates active threats.

### Incidents

An incident is any event that jeopardizes or threatens to jeopardize the integrity of your data; this includes events that violate privacy and other related laws and regulations. This can include unauthorized access, use of or changes to specific files, or to the network or any cloud app account belonging to your company.

### Mitigation

Mitigation is the act of taking steps to minimize the potential damage that can result from an active security breach. See also [remediation](#remediation).

### Protected, unprotected, and protectible users

Protectable users are users that fulfill two conditions: they have a mailbox and they are licensed (under either Microsoft 365 or Google Workspace). Protectible users currently are unprotected but can be provided protection by adding them to the list of protected users.

Protectable users for whom Coro has added protection are protected users; protectable users for whom Coro has not added protection are unprotected.

### Protected device

A protected device is one that is running an endpoint agent that has been installed using a workspace-specific link.
See [Deploy Coro Agent](../installation/coro-agent.md#deploy-coro-agent).

### Remediation 

Remediation is the act of correcting an error or stopping a security breach. See also [mitigation](#mitigation).

### Resolution 

Resolution is the act of indicating that you have completed actions that need to be taken for any ticket in Coro by clicking **Resolve**.

### Tickets

Tickets indicate an incident identified by Coro for any of the environments and objects protected by Coro; tickets store all information relevant to the incident. 

### Vulnerability 

Any weakness in your network that a bad actor can leverage in order to access your environments and harm your security posture. 