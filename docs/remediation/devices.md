
Coro detects files and processes on the endpoint devices suspected as malicious, as well as various vulnerabilities in the security posture of the devices. 

*   All files detected as malicious are automatically quarantined and all processes detected as suspicious are automatically terminated; no further remediation actions on the side of the operator are required. At the same time, after examining the ticket, the operator may decide to approve the respective file or even to exclude the folder in which the flagged file resided from malware scans by Coro. Therefore, all tickets on malware files/processes are classified as suggested for review (G2), with a review time window of two weeks.
    *   _Ignore_. No auto-remediation is performed, and the ticket is classified as G3, being auto-closed and logged immediately. 
    *   _Review_. No auto-remediation is performed, and the ticket is classified as requiring review (G1). The ticket remains open until either the operator closes it manually or the vulnerability is observed by the Coro endpoint agent as being resolved. 
    *   _Enforce_. Auto-remediation is performed, recorded in the ticket, and the ticket is classified as G3, and thus auto-closed. 

See also: [Ticket classification](../remediation/ticket-classification.md)    