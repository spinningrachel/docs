
# Settings tab

In this screen, you configure DLP settings for various applications and critical data types by clicking **Control Panel** > **Data Governance**.

The first two settings determine the applications that perform DLP monitoring: 

- Cloud applications
- Email

The other settings determine the data types that are monitored:

- Personally identifiable information (PII)
- Payment card information (PCI)
- Protected health information (PHI)
- Certificates, passwords and source code
- Custom sensitive keywords to monitor for

# Permissions tab

This screen provides additional settings. You can refine the level of access permissions for a data type and per a specific user or user group.

Click **Add Permission** to add additional definitions.