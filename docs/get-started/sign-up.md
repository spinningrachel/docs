When you sign up, Coro creates a dedicated Coro workspace for your organization, setting it up from the get-go for security and
privacy monitoring of your and your users' Microsoft 365 and/or Google Workspace accounts. This includes email activity, data storage and more. See [What happens at sign-up?](#what-happens-at-sign-up) for more.

!!! info "Prerequisites"
    To sign up, you'll need an enterprise email account for which you have admin privileges.

Once you're signed up, you can log in to Coro by way of your Microsoft 365 or Google Workspace account, as described [here](#login).

## Sign up 

To get started, sign up and create a workspace with your admin account:

- [Microsoft 365](#sign-up-with-microsoft-365)
- [Google Workspace admin account](#sign-up-with-google-workspace)

### Sign up with Microsoft 365

1. Navigate to: https://secure.coro.net/sign-up/

2. Click **Sign Up** under **Microsoft 365**.

    !!! note
        Make sure you're using an [enterprise Microsoft 365](https://www.microsoft.com/en-us/microsoft-365/enterprise) account for which you have admin privileges.

{>>Need to add results from clicking the square and update process accordingly - what loads? The dashboard? or directly to the Cloud apps configuration? .<<}

3. Select your preferred account.

    You will be redirected to Microsoft 365 and be prompted to authorize Coro's access to your user data. Once you authorize access, you will return again to the Coro interface.
    
    Coro creates a unique workspace for your organization. On creation of your workspace:

    {==- The user that performs the creation (and connects the company’s primary cloud app Microsoft 365 (or Google Workspace) is added to protection.
    - All protectable users from the primary cloud app used for account creation are synced.==}{>>In addition to this, need to add any other results and maybe relevant screenshots from clicking the square and update process accordingly - what loads? Permissions? Need to do anything?  .<<}


4. Now, start protecting users and data. Click **Cloud apps** and make the following connections:  

    1. Select at least one cloud service from the list.
    2. [Add at least one user to protection](../users-roles/#add-users-to-protection).

    Coro syncs the users in your domain and begins to monitor your workspace immediately.


    !!! note
        For more information, see [Cloud applications](../configure/cloud-apps.md).

    !!! note
        You must connect to at least one cloud app; alternatively, you can [install the Coro agent](../installation/coro-agent.md) and start monitoring your endpoints.


### Sign up with Google Workspace

1. Navigate to: https://secure.coro.net/sign-up/

2. Click **Sign Up** under **Google Workspace**.

    !!! note
        Make sure you're using a [Google Business Workspace](https://workspace.google.com/) email account for which you have admin privileges.

3. Select your preferred account. 

    You will be redirected to Google and be prompted to authorize Coro's access to your user data. Once you authorize access, you will return again to the Coro interface.
    
    Coro creates a unique workspace for your organization. On creation of your workspace:

    {==- The user that performs the creation (and connects the company’s primary cloud app Office (365 or Google Workspace) is added to protection.
    - All protectable users from the primary cloud app used for account creation are synced.==}{>>In addition to this, need to add any other results and maybe relevant screenshots from clicking the square and update process accordingly - what loads? Permissions? Need to do anything?  .<<}


4. Now, start protecting users and data. Click **Cloud apps** and make the following connections:
    1. Select at least one cloud service from the list.
    2. [Add at least one user to protection](../users-roles/#add-users-to-protection)

    Coro syncs the users in your domain and begins to monitor your workspace immediately.


    !!! note
        For more information, see [Cloud applications](../configure/cloud-apps.md).

    !!! note
        You must connect to at least one cloud app; alternatively, you can [install the Coro agent](../installation/coro-agent.md) and start monitoring your endpoints.


## Login

1. From any supported web-browser, navigate to: https://beta.coro.net/portal/dashboard.
2. Select an authentication service: Microsoft 365, Google, or provide email credentials.
3. Click **Sign In**.
    Once you log in, Coro's web-enabled [actionboard](actionboard.md) appears, similar to the following: 

    ![Dashboard](../assets/screenshots/dashboard.png){ align=left }


## What happens at sign-up?

{>> TBD: which approach do we document? <<}

Administrators should fully understand the processes that take place on sign-up. 

### Today's modus operandi
Now, there are two modes:

- Detection (free). In this mode, the system makes visible the issues that it detects. But no actions are possible, neither automatic, nor manual, by admin staff.
- Detection and Response, where the admin can take action to the tickets and also the system can take action instead of the admin.

A prospect signs up for Coro and creates a workspace. This is what happens immediately:

1. The admin effectively connects Coro to the Microsoft 365 account allowing Coro to act on this tenant.
2. Coro starts syncing information with the users in that Microsoft 365 account. 
3. The admin of the Microsoft 365 account automatically becomes the **only** protected user in the related Coro workspace. The admin can then add Coro protection to some or all protectable users in the account. Coro syncs those users.  This process can take several minutes and even several hours, depending on the number of users in your domain.
4. The workspace is subscribed to Detection mode only but automatically puts the customer on a free trial of Detection and Response.


### Proposed modus operandi

1. At sign-up, Coro does not begin the trial, but gives the admin the ability to start the trial at the moment of his/her choosing. The customer can stop and start the trial for the first 14 days to allow admin to fine-tune the system. (TBD: calendar days, or an accumulated 14x24 hours?).
2. Until the trial starts, the system operates in Detection mode only.
3. From sign-up, Coro protects **all** users in the admin's workspace.
4. With the start of the trial, the customer experiences the full Coro Detection and Response experience: detection and remediation.