

The Coro actionboard provides an at-a-glance security posture status of the current workspace and the environments and objects that you have configured for monitoring.

![Dashboard](../assets/screenshots/dashboard.png){ align=left }

The posture relates to:

- [Cloud apps](#cloud-apps)
- [Users](#users)
- [Emails](#emails)
- [Devices](#devices)
- [Data](#data)

To view additional summary details, click the relevant object.
The view scrolls down further on the page to highligt the relevant details. 
From there, click those details to drill down and view more granular details, and to work with related tickets.

!!! note
    The information shown in any area of the actionboard relates to _protected users_ only. For more information, read more [here](../users-roles/#add-users-to-protection) 

## Cloud apps

Coro automatically identifies and blocks suspicious data orginating in the Cloud for Salesforce, Dropbox, Box, and more!  

![Cloud apps status](../assets/screenshots/cloud-apps-status.png)

This area of the actionboard displays:

- A list of all Cloud apps for which the connection process has been initialized by the operator and which have not been disconnected by the operator.
If no cloud apps connected, the widgets shows an empty state view.
- For each Cloud app, one of three statuses is shown:

    | Appearance                                                                 | Meaning                                                          |
    | -------------------------------------------------------------------------- | ---------------------------------------------------------------- |
    | Green status                                                                        | App is connected and currently, there are no issues    |
    | List of issues, each associated with a number of issue instances and actions menu   | App is connected and currently, there are issues       |
    | Red status associated with a call for action to establish or repair the connection  | App is partly connected                                |

- The latest **open** Cloud app tickets and tickets that have been resolved in the last 90 days.
- **Resolve** buttons that let you quickly handle open tickets.
- **All** links that bring you to the [Ticket Log](../tickets/ticket-log.md), filtered for Cloud apps.

See also:

- [Cloud applications](../configure/cloud-apps.md) to connect your cloud apps for protection
- [Cloud apps tickets remediation logic](../remediation/cloud.md)
- [Possible Cloud apps violations](../tickets/vulnerabilities-errors.md#cloud-apps)

## Users

Coro automatically identifies and blocks specific data that was created by users: insider threats, accounts hacking, and malicious activities.

![Users status](../assets/screenshots/users-status.png)

This area of the actionboard displays:

- The total number of users and a breakdown of protected and unprotected users. Click **View** to jump to a detailed view of the [protected users](../configure/protected-users.md). 
- The latest **open** user tickets and tickets that have been resolved in the last 90 days.
- **Resolve** buttons that let you quickly handle open tickets.
- **All** links that bring you to the [Ticket Log](../tickets/ticket-log.md), filtered for users.

See also:

- [Protected users](../configure/protected-users.md) to protect users and your organization 
- [User tickets remediation logic](../remediation/users.md)
- [Possible user violations](../tickets/vulnerabilities-errors.md#users)

## Emails

Coro scans every email for phishing, malware, and ransomware and automatically eliminates threats.

![Email status](../assets/screenshots/email-status.png)

This area of the actionboard displays:

- The total number of emails processed in the last 90 days, along with the top suspicious sources.
- The latest **open** email tickets and tickets that have been resolved in the last 90 days.
- The list of top suspicious domains, sorted in descending order of counters.
- **Resolve** buttons that let you quickly handle open tickets.
- **All** links that bring you to the [Ticket Log](../tickets/ticket-log.md), filtered for emails.

See also:

- [User tickets remediation logic](../remediation/emails.md)
- [Possible email violations](../tickets/vulnerabilities-errors.md#emails)

## Devices

The Devices area displays activity on the devices, or endpoints (laptops, virtual machines, or PCs) on which the [Coro Agent](../installation/coro-agent.md) is installed. The Coro Agent monitors sensitive data, practice data, and user activity, enabling you to enforce a wide range of security, compliance, and governance policies on non-network devices.

![Device status](../assets/screenshots/device-status.png)

This area of the actionboard displays:

- The total number of devices in the workspace and a breakdown of devices with and without vulnerabilities.
- The latest **open** device tickets and tickets that have been resolved in the last 90 days.
- **Resolve** buttons that let you quickly handle open tickets. The menu contains repair actions that are appropriate to the ticket.
- **All** links that bring you to the [Ticket Log](../tickets/ticket-log.md), filtered for devices.

See also:

- [Endpoints](../configure/endpoints.md) to connect and protect your devices
- [Device tickets remediation logic](../remediation/devices.md)
- [Possible devices violations](../tickets/vulnerabilities-errors.md#devices)

## Data
Coro scans every file and email, or any data share for PII/PCI/PHI, and prevents confidential information leakage.

![Data status](../assets/screenshots/data-status.png)

This area of the actionboard displays:

- The total number of data objects that were processed for DLP (such as files and folders) over the last 90 days, along with top violators, for example users who shared something outside of the company.

    - The view separates between sharing from a Cloud drive, sharing by email, and copying to unallowed location on endpoint disk
    - The view separates between PII, PCI, and PHI. For each of these cross-categories (for example, PII by email), the operator can view the list of respective unprocessed events.

- The latest **open** data tickets and tickets that have been resolved.
- The list of top users with permission violations, sorted in descending order of counters.
- **Resolve** buttons that let you quickly handle open tickets.
- **All** links that bring you to the [Ticket Log](../tickets/ticket-log.md), filtered for data.

See also:

- [Data](../configure/data.md) and [Data governance](../configure/data-governance.md) to configure protection for your company's data 
- [Device tickets remediation logic](../remediation/data.md)
- [Possible devices violations](../tickets/vulnerabilities-errors.md#data)