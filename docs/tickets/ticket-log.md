
The Ticket Log displays every ticket reported for the current workspace.

View the Ticket Log by clicking **Ticket Logs** in the toolbar.

!!! note
	You can also access the Ticket Logs by clicking the relevant ticket link from other areas across Coro, including the **Actionboard**, **Devices**, and **Users** areas. 

![Dashboard](../assets/screenshots/ticket-log.png){ align=left }

The left pane shows a summary of the tickets. Click a ticket summary to see ticket details in the right pane.

- Information describing the ticket is provided, such as: the user affected, the date the ticket was opened, the file name and size, and more. 
- An **Actions** button opens a dynamic menu with possible actions you can perform on each relevant ticket. Click an action to execute it. For example, let's say you have enabled malware protection for Cloud drives. Any ticket of type **Malware in Cloud Drive** displays the **Approve file** option in the **Actions** menu. Selecting that action automatically moves the file from the Suspected folder to the previous folder, and the file becomes approved. 

	See [Actions for ticket types](#actions-for-ticket-types) for a full description of possible actions in the Ticket Log.

- At the bottom are further ticket details and a list of activities that have been performed on this ticket.

You can display all tickets, only open tickets, or only closed ones.

You can also filter the Ticket Log according to:

- Security object (select **Everywhere** for all objects)
- Ticket type
- Time period. You can select a predefined period, for example, **Last 3 days**, or specify a period by using the calendar.

You can also search the log using free text.

## Actions for ticket types

The following tables list the actions available for the Coro ticket types and describe the expected outcomes of choosing those actions.

### Cloud apps


|**Ticket type**|**Actions available**|**Outcomes**|
| :- | :- | :- |
|**All ticket types**|Close ticket|All tickets related to the selected device should be closed. No remediation actions should be taken. No open tickets should be displayed in Tickets section for the device.|
|**Abnormal admin activity**|Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Request user to sign-in to all cloud apps|A record is added in Activity Logs:<br>USER\_NAME was requested to re-login to all protected cloud applications that they are using.|
||Request user to sign-in to <*cloud service*>|A record is added in Activity logs: USER\_NAME was requested to re-login to CLOUD\_APP.|
||Add protection|**TBD**|
||Approve file|The file from the "Suspected folder"  is moved back to the main folder.|
||Delete file| TBD |
|**Malware in cloud drive**|Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Request user to sign-in to <*cloud service*>|A record is added in Activity logs: USER\_NAME was requested to re-login to CLOUD\_APP.|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
||Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Request user to sign-in to all cloud apps|Added record in Activity Logs:<br>USER\_NAME was requested to re-login to all protected cloud applications that they are using.|
|**Suspected identity compromise**|Request user to sign-in to <*cloud service*>|A record is added in Activity logs: USER\_NAME was requested to re-login to CLOUD\_APP.|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|


### Users


|**Ticket type**|**Actions available**|**Outcomes**|
| :- | :- | :- |
|**All ticket types**|Close ticket|All tickets related to the selected device should be closed. No remediation actions should be taken. No open tickets should be displayed in Tickets section for the device.|
||Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Request user to sign-in to all cloud apps|A record is added in Activity Logs:<br>USER\_NAME was requested to re-login to all protected cloud applications that they are using.|
||Request user to sign-in to <*cloud service*>|A record is added in Activity logs: USER\_NAME was requested to re-login to CLOUD\_APP.|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
|**Mass data download**|Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Request user to sign-in to all cloud apps|Added record in Activity Logs:<br>USER\_NAME was requested to re-login to all protected cloud applications that they are using.|
||Request user to sign-in to <*cloud service*>|A record is added in Activity logs: USER\_NAME was requested to re-login to CLOUD\_APP.|
||Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended</p>|
|**Suspicious exposure of certificate**|Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
||Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
|**Suspicious exposure of critical data**|Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
||Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended**.**</p>|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
|**Suspicious exposure of password**|Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
||Un-log and remove from audit reports|**TBD**|
|**Suspicious exposure of source code**|Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Request user to sign-in to all cloud apps|Added record in Activity Logs:<br>USER\_NAME was requested to re-login to all protected cloud applications that they are using.|
||Request user to sign-in to <*cloud service*>|A record is added in Activity logs: USER\_NAME was requested to re-login to CLOUD\_APP.|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
|**Suspected bot attacks**|Un-log and remove from audit reports| TBD |


### Emails


|**Ticket type**|**Actions available**|**Outcomes**|
| :- | :- | :- |
|**All ticket types**|Close ticket|All tickets related to the selected device should be closed. No remediation actions should be taken. No open tickets should be displayed in Tickets section for the device.|
|**Malware in email attachment**|Close ticket|All tickets related to the selected device should be closed. No remediation actions should be taken. No open tickets should be displayed in Tickets section for the device.|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
||Approve this email|<p>The email is moved from the Suspected folder to the Inbox. </p><p>The sender will not be allowlisted.</p>|
||Delete this email|<p>The email is deleted from the Suspected folder.</p><p>The sender will not be blocklisted.</p><p>A record is added in the Activity and Ticket Log: Email from <sender's email> has been deleted.</p>|
||Add sender to blocklist|<p>The email address is added to the blocklist.</p><p>If was in the allowlist, then it is removed from the allowlist.</p><p>The email from the sender is deleted for any receiver.</p>|
||Add sender's domain to blocklist|<p>The domain is added to the blocklist. </p><p>If it was in the allowlist, then it is removed from the allowlist.</p><p>Emails from this domain are moved from the Inbox to the Suspected folder for all users of the customer.</p>|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
||Approve this email|<p>The email is moved from the Suspected folder to the Inbox. </p><p>The sender will not be allowlisted.</p>|
||Delete this email|<p>The email is deleted from the Suspected folder.</p><p>The sender will not be blocklisted.</p><p>A record is added in the Activity and Ticket Log: Email from <sender's email> has been deleted.</p>|
||Add sender to allowlist|<p>The email address is added to the allowlist. </p><p>Emails from this address are moved from the Suspected folder to the Inbox for all users of the customer.</p>|
||Add sender to blocklist|<p>The email address is added to the blocklist.</p><p>If was in the allowlist, then it is removed from the allowlist.</p><p>The email from the sender is deleted for any receiver.</p>|
||Add sender's domain to allowlist|<p>The domain is added to the allowlist.</p><p>If it was in the blocklist, then it will be removed from the blocklist.</p><p>Emails from this domain are moved from Suspected to Inbox for all users of the customer.</p>|
||Add sender's domain to blocklist|<p>The domain is added to the blocklist. </p><p>If it was in the allowlist, then it is removed from the allowlist.</p><p>Emails from this domain are moved from the Inbox to the Suspected folder for all users of the customer.</p>|

### Devices


|**Ticket type**|**Actions available**|**Outcomes**|
| :- | :- | :- |
|**All ticket types**|Close ticket|All tickets related to the selected device should be closed. No remediation actions should be taken. No open tickets should be displayed in Tickets section for the device.|
|**Firewall disabled**|Enable firewall|The ticket is closed. The firewall is enabled on the machine. An action: "Firewall was re-enabled" is added to the Action Log.|
|**UAC notification missing**|Enforce UAC notification|UAC is enabled on the machine. An action: "UAC was re-enabled" is added to the Action Log.|
|**Device password missing**|Close ticket|All tickets related to the selected device should be closed. No remediation actions should be taken. No open tickets should be displayed in Tickets section for the device.|
|**Malware on endpoint**|Remote scan|<p>Remote scan starts. A Coro notification saying that the remote scan was started is displayed on the device. A record is added in the Coro Agent located at: C:\ProgramData\CoroAgent1SRV\Logs saying that the remote scan has started.  If there are eicar files on your device, they are found and quarantined by the remote scan.</p><p>The folders that are scanned:</p><p>C:\ProgramData</p><p>C:\Program Files</p><p>C:\Program Files (x86)</p><p>C:\Users\Public C:\WINDOWS</p><p>Files with these extensions are scanned:<br>".exe", ".dll", ".lnk", ".bat", ".com", ".cmd", ".pif", ".sys", ".js", ".html", ".ini", ".tlb", ".dat", ".bin", ".ps", ".ps1", ".py", ".py3", ".pyw", ".pyd", ".pxd", ".pyz", ".pywz", ".txt", ".zip"</p>|
||Approve this file|The file from the "Suspected folder"  is moved back to the main folder.|
||Exclude folder from malware scan||
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
|**Development mode enabled**|Disable developer mode|Developer mode is disabled on the machine. An action: "Developer mode was disabled" is added to the Action Log.|
|**Non-genuine Windows copy**|TBD|**TBD**|
|**Infected process**|Remote scan|See "remote scan" under "Malware on endpoint".|
||Approve process group|**TBD**|
|**Unencrypted endpoint drive**|Encrypt drive|<p>A record: "Drive encryption was requested on [users machine] by [user]" in the Activity Log.</p><p>When encryption is complete, a record is added: "Drive was encrypted on [users machine] of [user]" in the Activity Log.</p>|
||Allow no encryption|**No description**|
|**VSS backup protection**|Remote scan|See "remote scan" under "Malware on endpoint"|


### Data


|**Ticket type**|**Actions available**|**Outcomes**|
| :- | :- | :- |
|**All ticket types**|Close ticket|All tickets related to the selected device should be closed. No remediation actions should be taken. No open tickets should be displayed in Tickets section for the device.|
||Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
|**PCI (payment card information)**|Close ticket|All tickets related to the selected device should be closed. No remediation actions should be taken. No open tickets should be displayed in Tickets section for the device.|
||Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
|**PHI (protected health information)**|Close ticket|All tickets related to the selected device should be closed. No remediation actions should be taken. No open tickets should be displayed in Tickets section for the device.|
||Suspend user from all cloud apps|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his account on CLOUD\_APP is temporary suspended.</p>|
||Suspend user from <*cloud service*>|<p>A notification: "User's access to cloud app has been suspended", and "Users updated" is displayed.</p><p>The access of USER\_NAME to her/his accounts on all protected apps will be temporarily suspended.</p>|
||Remove exposing sharing| TBD |
||Contact user|<p>An email from Coro with ticket info and message is sent to the recipient.</p><p>An action: "Contact User" is recorded in the Ticket and in the Activity log.</p>|
