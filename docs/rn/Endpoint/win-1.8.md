Coro Agent for Windows, version 1.8 provides new features, enhancements, and fixes:

## New features

- EDR
- Shutdown and reboot device actions
- Device isolation from network
- GZIP support for EDR data
- Registry changes monitoring per machine and user

## Enhancements

- Malware detection after Stop scan
- Password detection fails on rare systems
- Isolation mode works for windows7
- Correct machineId calulation

## Known issues

- Isolation mode not working on Windows 7