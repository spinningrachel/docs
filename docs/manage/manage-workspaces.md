---
template: overrides/main.html
---

## Workspace types

There are three types of Coro workspaces.

|Workspace    |Description|
|--------------|-----------|
|Regular    |Created through domain administrator sign-up or by using an MSP tool. In use in most organizations. There is typically a single workspace per organization. This workspace type can view Billing in the web console.|
|Channel    |Created only by MSPs or ISPs. Contain several child workspaces. Does not have Billing in the web console (though Billing plan can be set up directly in Recurly, the Coro payment system). See [Channel Workspace permissions](#channel-workspace-permissions). |
|Child    |Belongs to a Channel workspace. Does not have Billing in the web console (though Billing plan can be set up directly in Recurly, the Coro payment system).|

## Channel Workspace permissions

Administrators of Channel workspaces have the following permissions:

- They can create a child workspace.
- They can log in to any child workspace and can view all child workspaces. The details that are displayed and that are editable are:
    - Workspace ID and display name
    - Current billing plan (trial, freemium, or premium)
    - Time remaining for the current plan
    - Maximum number of protected users (by default, there is no maximum value)
    - Number of protected users
    - Number of protectable users
    - Number of "red" issues
    - Days since the last activity in the actionboard
    - Notifications policy: All admins / Only channel admins / No notifications [default]

## Manage workspaces

If you have access to more than one workspace, you can manage them by clicking **Manage Workspaces** in the toolbar.

!!! note
The **Manage Workspaces** button is available only for MSP clients.

![Dashboard](../assets/screenshots/manage-workspaces.png){ align=left }

For each workspace, the following details are displayed:

- Workspace name
- The billing plan for the workspace
- Days left in the current billing plan/cycle?
- The number of workspace users who are protected under the current billing plan
- The maximum number of protected users: the total number of users in the workspace that are protected
- The number of protectable users: the total number of users in the workspace that are not protected
- The number of open tickets in the workspace
- The most recent login timestamp for the workspace

### Filter workspaces

You can filter the workspaces according to plan or type. You can also search for the name of workspace by typing its name, or part of it, in the search field. 

### Create a workspace

To create a workspace:

1. Click **Manage Workspaces** > **Create**. 
2. Select **Channel Workspace** or **Regular Workspace**.


![Create workspace](../assets/screenshots/create-workspace.png){ align=left }


**For a Regular Workspace**

Specify:

- Company name
- Display name
- Domain name
- Administrator's email

You can also specify who receives the notifications that are issued by the Workspace:

- No-one
- Only administrators at Coro, as well as users of the Workspace
- All administrators

You can also add graphic content to your workspace settings:

- Header icon: This icon appears in the banner of the Coro window.
- Email icon: This is the icon that appears in notification emails.

**For a Channel Workspace**

In addition to the information for the regular Workspace, you can also specify:

- Subdomain
- Brand color: Specify a HEX color. For example, #FF5733. 
- No-reply email: The address to use for "sent by" fields in notification emails
- Support email: The email address of the company's support personnel
- Company address

### View or edit a workspace

There is a menu at the extreme right of each Workspace in the list. 

![Dashboard](../assets/screenshots/edit-view-workspace.png)

From the menu:  

- Select **Edit Workspace** to display the workspace settings. Edit settings as needed and click **Save** to apply them.
- Select **View Workspace** to display details of all the security objects in the workspace.