This screen displays the protected users and protected user groups in the workspace. 

Access the screen by clicking **Control Panel** > **Protected Users**.


<!--get a better screenshot -->
![Dashboard](../assets/screenshots/protected-users.png)


## Users tab 

The following data is provided for each protected user:

- User name
- The method that was used to add protection for the user
- The date on which the protection was added

You can search existing protected users, add new users, and remove them.

## Groups tab

The following data is provided for each Microsoft 365 and Google group:

- Group name
- The number of users in the group
- The date on which the protection was added

You can search existing user groups, add new groups, and remove them.

To perform an action for all members of a group, choose a group, then click an action from the **Action** menu.

<!--what is in the mini-menu at the end of each line? -->