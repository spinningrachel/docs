

Coro version 1.7 provides many new features, enhancements, and fixes:

## New features 

This section describes the new features that we're releasing with version 1.7.

### Remote disk encryption

!!! note
    This feature is supported on installed drives only. It is not yet available for removable drives.

Coro now enables remote disk encryption for MS Windows endpoint devices. When you encrypt an endpoint remotely, the encryption key is automatically stored in the device details from the Coro Console.

For any device, you can see the list of drives and their statuses under **Actionboard > Devices > View**.

Additionally, tickets are generated for the vulnerability _Unecrypted Endpoint Drive_ for supported endpoints when at least one of their drives is unencrypted. In this case, the device details per drive also appear in the ticket itself.

For more information, see [Endpoint encryption](http://127.0.0.1:8000/bugatti/product/pmm-nd-docs/remediation/endpoint-encryption/).

The **domain** and **violater** search operators are now available from a dropdown in the search field from **Ticket Logs**. This is the first of many additional operators we plan on adding.

## Enhancements 

Following are enhancements to existing features that we're releasing with version 1.7:

* The user interface for Coro Agent on Windows has been improved.
* Search filters have been added for the Devices page.
* Last known IP and UTC was added to the device details in the **Devices** area.
* Google and Office email message IDs now appear in email tickets and notifications.

## Fixed issues 

* After an email is marked as safe, there is now an Activity Log message that the email has been moved to the Inbox.
* The first time an admin is invited to a workspace, they are also added as a protected user.
* When the admin deletes a group, the admin themselves is not deleted.
* Specific sensitive keyword combinations that include a space now trigger tickets.
* Clicking All activity in a specific ticket in the Ticket Log now correctly displays all activity relating to the specific ticket only.
* In keeping with Microsoft's naming, Coro has updated all "Office 365" labels in the Coro Console to "Microsoft 365". Nothing has changed with respect to functionality.
* When filtering ticket types in the Ticket Log, types are displayed in the same order in which they are displayed in the corresponding Actionboard areas. For example, if you filter on Users in the Ticket Log, the options in Types are in the same order in which they are displayed in the Users area under the Actionboard.
* When device users resolve a vulnerability (for example: Device Password Missing) themselves, Coro recognizes the fix and the related ticket is now closed automatically.
* The "When" section in email tickets now reflects the event creation time and not the sent time.

### Miscellaneous updates 

We've made many changes and improvements across the Coro Console, including fixes and enhancements to the UX, improved messaging, fixes to the sign-in flow for SSO users, additional allow-listed processes and other similar items.