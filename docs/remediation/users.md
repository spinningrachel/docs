
*   Access Permission violations for which an operator has already explicitly specified an auto-remediaton action are immediately closed.
*   All other user tickets are classified as suggested for review (G2), with time windows of 1-2 weeks, depending on the possible lasting effect of the respective detection.
*   At this time, Suspected Bot Attack and Abnormally Massive Dowload tickets are currently classified  as G3 and are automatically closed, however Coro is currently evaluating this logic and considering aligning them with the other tickets in the Users category.

See also: [Ticket classification](../remediation/ticket-classification.md)