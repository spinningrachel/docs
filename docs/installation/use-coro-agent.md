!!! tip
    This page provides instructions for the users in the workspace on whose PCs Coro Agent is deployed. Instructions for administrators for deploying and administrating the Coro Agent are provided [here](../installation/coro-agent.md). 

Coro Agent detects vulnerabilities on your PC. In addition to providing anti-virus and anti-malware protection, it checks more, such as if your computer has a password, that Developer Mode remains turned off, and that your computer drives are encrypted.

The Coro Agent notifies you about issues that have been detected by Coro and are being remediated by the Coro system or the administrator. These notifications can help you better understand why files in your hard drive may be missing, or why your computer is working slowly.

The Coro Agent also notifies you about a few issues that only you can remediate yourself, such as adding a password to the computer.

## Open Coro Agent

### Windows

To display the Coro Agent, in the Windows system tray (usually located in the bottom right corner), click the Coro icon:

![System tray](../assets/screenshots/windows_system_tray.png)

Coro Agent is displayed:

![Agent UI notifications](../assets/screenshots/Coro-Agent-notifications.png)

### Mac

[TBD]

## Notifications in Coro Agent

The following are some of the common notifications that could be displayed in the Coro Agent interface:

- Malware has been detected and quarantined
- Infected process has been detected
- A new software version is available
- Starting device malware scan
- Remote scan was started by your admin
- Malware scan has ended
- Your drive is not encrypted
- Your device password is missing
- Developer mode is enabled
- Your operating system isn't genuine
- Your firewall is turned off
- UAC mode disabled
- Scan interrupted
- Coro protection enabled
- Malware removed
- Zip file downloaded successfully

## Deploy Coro Agent

There are two methods for installing Coro Agent.

- **For single users**: Coro provides a link to the installer. Admin personnel email you the link. You click the link and follow the instructions to install.
- **For use with mass deployment tools, such as GPO**: Admin staff use the same link to download the Agent, then deploy it en masse using the deployment tool. 

Either method can be used for devices running Windows and macOS.

See also: [Agent deployment](../configure/endpoints.md#agent-deployment)

## Upgrade Coro Agent

Periodically, Coro displays a notification that a new version of Coro Agent is available.

![auoto-upgrade-notification](../assets/screenshots/coro-agent-auto-update-notification.png)

Click **Next** to begin the upgrade.