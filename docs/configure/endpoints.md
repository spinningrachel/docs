Configure protection for endpoints by clicking **Control Panel** > **Endpoint Devices**.

This screen lets you configure protection on PCs that have a [Coro Agent](../installation/coro-agent.md) installed. There are three tabs:

## Agent deployment

This window presents several versions of Coro Agent for Windows and macOS. Information (whether stable or in beta, the number of devices on which the version is currently deployed) and release notes are provided for each to help you make the choice that best meets your needs.

![Endpoint configuration](../assets/screenshots/endpoint-devices-configuration.png)

Click the gear icon in the Agent Deployment window to filter all release channels or stable channels. 

![Select channels](../assets/screenshots/stable-all-release-channels.png)

From the **Actions** menu for each version, you can either:

- Download [Coro Agent](../installation/coro-agent.md) installation software, usually if you are going to mass-deploy the Coro Agent or
- Get the URL to the software, usually to share with specific end users, so that they can install the Agent directly onto their devices.

To have Coro notify you of forthcoming Coro Agent updates, click the relevant button.

## Settings

From here, you can make settings related to endpoint monitoring using Coro Agent. The settings apply to all devices in the workspace.

- **Heartbeat interval**: Defines how often Coro pings endpoints to monitor their security status.
- **Tamper Protection**: Turn this option on to prevent users and certain types of malware from tampering with important security features and stopping the protections.
- **Notify Users when Coro Agent Update is Available**: User notification is turned off when the agent updates are performed in a centralized manner, using mass deployment tools.
- **Advanced Threat Control**: When turned on, Coro monitors active processes for known and potential threats and blocks processes that exhibit suspicious behavior while not being explicitly allowlisted.
- **VSS Backup Protection**: When turned on, Coro enforces backup snapshots every four hours and blocks processes that exhibit risks to the backup while not being explicitly allowlisted.
- **Enhanced EDR Block Mode**: When Coro Endpoint Protection is used side by side with Windows Defender Antivirus, it provides added endpoint detection and response (EDR) from malicious artifacts. Enhanced EDR block mode stratifies this added protection by ensuring access to timely data that may otherwise be supressed by the environment.

## Device posture

The device posture profile is a set of criteria that a user's device (laptop    , virtual machine, or PC) must meet in order to access applications.

Use this screen to set the trust policy for computers in your workspace that are running Windows PC, Windows Server, or macOS. According to the policy enforced, Coro collects and inspects security-related data from the connected devices. These are the criteria that constitute a device posture:

- UAC Notification Missing
- Device Password Missing
- Firewall Disabled
- Unencrypted Endpoint Drive
- Development Mode Enabled
- Non-geneuine Windows Copy 

For each, select one of the following:

- **Ignore**: No auto-remediation is performed, and the ticket is classified as G3, being auto-closed and logged immediately. 
- **Review**: No auto-remediation is performed, and the ticket is classified as requiring review (G1). The ticket remains open until either the operator closes it manually or the vulnerability is observed by the Coro endpoint agent as being resolved. 
- **Enforce** Auto-remediation is performed, recorded in the ticket, and the ticket is classified as G3, and thus auto-closed. 

See also: [Ticket classification](../remediation/ticket-classification.md)