**Installing the Outlook Add-On**

{TBD: this is odd material - where does it come from. Also: there are links in here to installation/media/<X>.png}

![A picture containing sign, light Description automatically
generated](media/image1.png){width="1.104861111111111in"
height="1.1277777777777778in"}

 

**Easily report suspicious emails to IT administrators.**

The purpose of the Coro add-on for Microsoft 365 is to allow end users
to report to their IT personnel on suspicious emails in their Inboxes,
as well as to report on emails that have been flagged by the Coro
protection and moved to Suspected folder yet the user believes that the
flagging was unjustified.

The feedback provided by the end users via add-on is communicated to the
IT personnel via the Coro Actionboard as Email security tickets that
require admin's review. After examining the ticket, the IT administrator
may decide to adopt users\' feedback by either approving/discarding the
respective email or allowlisting/blocklisting the sender of the email in
question, or even allowlisting/blocklisting the entire email domain of
the sender.

The installation of the add-on is completely transparent to the end
users. As an IT administrator, you can install the add-on for all your
Microsoft 365 users at once by following these steps:

1.  Make sure you are logged into Microsoft 365 with your global admin
    > credentials.

2.  In your browser, go to the [Coro page on Microsoft
    > AppSource ](https://appsource.microsoft.com/en-us/product/office/WA200003312)web
    > site.

3.  Select \"Get it Now\"

 

![Graphical user interface, application Description automatically
generated](media/image2.png){width="6.5in" height="3.25in"}

    4. Accept the requested permissions

![Graphical user interface, application, Teams Description automatically
generated](media/image3.png){width="6.5in" height="4.854166666666667in"}

5\. Once installed, you will be directed to your Microsoft 365 admin
panel to configure your settings. We advise that you assign the Coro
add-on for the entire organization. 

     a.

![Graphical user interface, application, Teams Description automatically
generated](media/image4.png){width="6.5in"
height="3.3291666666666666in"}

     b.

![Graphical user interface, application Description automatically
generated](media/image5.png){width="6.5in" height="3.859027777777778in"}

 

**What does the Coro add-on look like for end users?**

7\. Users will now see the Coro icon within their Outlook App for the
web and desktops.

 

![Graphical user interface, application Description automatically
generated](media/image6.png){width="6.5in" height="5.250694444444444in"}

   8. An instructions box pops up when the user clicks the Coro icon,
and the users can now report phishing and/or mark emails as safe.

 

![Graphical user interface, application Description automatically
generated](media/image7.png){width="6.5in"
height="5.7756944444444445in"}
