
{TBD: https://gitlab.com/groups/coronet/bugatti/-/epics/42
Is that detail required?}

The Ticket Log includes all tickets from all areas of the system. Tickets are handled and displayed as follows:

- Automatic ticket resolution is performed on many tickets, reducing the amount of manual work for operators.
- Enriched details provide further context around events.
- Advanced filtering enables sorting and searching for specific tickets and groups of tickets. 

## Automatic ticket resolution

All tickets related to issues that don't need to be reviewed by an operator are closed automatically. This feature makes it more intuitive and easier to manage the number of open tickets in the system at any given time.

### Protected and unprotected user tickets
In general, all tickets created in relation to users:

- For protected users, tickets are logged and referenced for audit once closed; this logging activity is communicated to the operators in the ticket view.

- For unprotected users, Coro continues to generate tickets related to unprotected users, and these tickets are only accessible through the Ticket Log view. Additionally, these tickets are:
   - Automatically closed by Coro and cannot be re-opened
   - Not logged nor referenced for audit.
   
     !!! note
         Coro has no obligation to keep these tickets accessible over time.

Tickets are identified and handled by Coro according to the following classifications:

*   Require operator’s review (G1) - tickets remain open until the operator explicitly closes them
*   Suggested for operator’s review (G2) - tickets remain open for a limited period of time and are automatically closed by the system when the respective time window for review is over, whether the operator reviewed them or not
*   Immediately auto-closed by the system (G3) - tickets are closed immediately by Coro, either following their resolution or because the related issues only need to be monitored and logged for compliance audits

**How it works**

1.  Once created in the system, tickets are sorted and assigned to their relevant area: email, data, devices, users, cloud apps.
2.  Tickets are classified according to the unique logic for that area.
3.  If a closed ticket is explicitly re-opened by the operator, it is automatically re-classified independent of its initial classification by the system.

Once Coro has recognized a phishing email or email malware, it moves the email to the **Suspected** folder.

After two weeks, the email moves to **Recover deleted items**. Thereafter, the email is permanently deleted after a period that is specified by the administrator (this is not a Coro feature).

Alternatively, if you use the **Discard all emails from this sender** option through the console, the email moves directly to **Recover deleted items**.

See the particulars of the logic of automatic remediation for different security assets:

- [Cloud apps tickets](../remediation/cloud.md)
- [Users tickets](../remediation/cloud.md)
- [Emails tickets](../remediation/emails.md)
- [Devices tickets](../remediation/devices.md)
- [Data tickets](../remediation/data.md)