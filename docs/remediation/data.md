
Ticket logic for data violations attempts to find the balance between the need to:

*   Detect and log everything for audit that might constitute a privacy/compliance breach. 
*   Primarily focus on particularly problematic private information exposures.

Accordingly, data governance tickets are classified as follows:

*   Detections that, according to the best practices of data governance regulations (GDPR, HIPAA, SOC2, etc.), require the attention of the data compliance officers - tickets are classified as suggested for review (G2), with a review time window of two weeks.
*   All other tickets are classified as G3, and are automatically closed by the system.

See also: [Ticket classification](../remediation/ticket-classification.md)